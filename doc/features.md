# Update the features

All features are listed in a single yaml file
([`/data/features.yml`](/data/features.yml)) under the `features` section.

## Update the features page (under `/features`)

The [`/features`](https://about.gitlab.com/features/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

The table below tries to describe the minimum values a feature that appears
under the `/features` can have and how they should be used.

| Features values | Description |
| --------------- | ----------- |
| `title` | The distinct title of a feature.  |
| `description` | The description of the feature. Can include Markdown. |
| `screenshot_url` | Relative URL of the feature screenshot if relevant. |
| `link_description` | Below every feature usually a link appears pointing to the feature page or the documentation. This value is its description, e.g., `"Learn more about CI/CD"`. |
| `link` | The link pointing either to the feature page or in the docs. |
| `gitlab_ce`  | Set to true if the feature is in CE. |
| `gitlab_ees` | Set to true if the feature is in EES. |
| `gitlab_eep` | Set to true if the feature is in EEP. |
| `not_on_gitlab_com` | Set to true for features which aren't relevant to GitLab.com, for example LDAP authentication. |
| `feature_page` | Set to true for pages that have their own dedicated page under `/features`. If set to true, the `link` should point to the relevant feature page. All features that have this and `gitlab_ce` set to true, are shown on the Products page under CE (because CE has about as many features in the `features.yml` as EES, it made EES look less enticing, so we currently only show features that have a feature page). |
| `solution`  | Every feature must have a solution (category) otherwise CI will fail. Check [`data/solutions.yml`](../data/solutions.yml) for a list of existing ones. |

Example:

```yaml
  - title: "Distinct feature name"
    description: "Feature description"
    screenshot_url: "images/feature_page/screenshots/09-gitlab-ci.png"
    link_description: "Learn more about feature name"
    link: link to docs or feature page
    solution: efficiency
    gitlab_ce: true
    gitlab_ees: true
    gitlab_eep: true
```

## Update the products page (under `/products`)

The [`/products`](https://about.gitlab.com/products/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

It works the same as the features description above.

## Update the gitlab-com page (under `/gitlab-com`)

The [`/gitlab-com`](https://about.gitlab.com/gitlab-com/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

It works the same as the features description above.

## Create or update the comparison pages (under `/comparison`)

The [comparison page][comp] grabs its content automatically from
`data/features.yml`.

There are 2 files in total which you need to create or update:

- `data/features.yml`: Update for new comparisons. Comparisons are automatically
  generated from the contents of this file.
- `source/comparison/gitlab-vs-competitor.html.haml`: Create for new comparisons.
  Every comparison page has its own html file (**use dashes**).

1. Edit `data/features.yml` (`competitors`, `comparisons` and `features`
   sections):

    ```yaml
    competitors:
      gitlab_com:
        name: 'GitLab.com'
        logo: '/images/comparison/gitlab-logo.svg'
      github_com:
        name: 'GitHub.com'
        logo: '/images/comparison/github-logo.svg'

    ...

    comparisons:
      gitlab_com_vs_github_com:
        title: 'GitΗub.com vs. GitLab.com'
        link: '/comparison/gitlab-com-vs-github-com.html'
        product_one: 'gitlab_com'
        product_two: 'github_com'
        pdf: 'gitlabcom-vs-githubcom.pdf'

    ...

    features:
      - title: "Briefly explain the feature"
        description: |
          Describe the differences in detail. This text can span in multiple
          lines without interfering with its structure. It will always appear
          as one paragraph.
        link_description: "Learn more about Feature Name."
        link: "link to GitLab's feature page documentation or blog post"
        solution: efficiency
        gitlab_ce: true
        gitlab_ees: true
        gitlab_eep: true
        gitlab_com: true
        github_com: true
    ```

      **Notes:**
      - The `link` attribute is the relative link to the comparison page. It should
        have the same name as the one we create in the next step.
      - The `pdf` name should be similar to the yaml file name, but with dashes. In
        short, it has to match the HTML page name in step 2. If you want to omit the
        PDF link altogether, set its value to `null`, like: `pdf: 'null'`.
      - The competitor's logo (`product_two`) can be `svg` or `png`. Save it in
        `source/images/comparison/competitor-logo.svg`.
      - In the features area, `product_one` is always GitLab, and `product_two`
        is the competitor we are comparing against. Values for these two fields are
        `true|false|partially`.

2. Add a new haml file named after the comparison using dashes:
   `source/comparison/gitlab-vs-competitor.html.haml`. You can start by copying
   an existing one and then editing it. The only change you need to make is the
   names of competitors:

    ```yaml
    ---
    layout: comparison_page
    trial_bar: true
    suppress_header: true
    title: "GitHub.com vs GitLab.com | GitLab compared to other tools"
    image_title: '/images/comparison/title_image.png'
    extra_css:
      - compared.css
    extra_js:
      - comparison.js
    ---

    - competitors = data.features.competitors
    = partial "includes/comparison_table", locals: { comparison_block: data.features.comparisons.gitlab_com_vs_github_com, product_one: competitors.gitlab_com, product_two: competitors.github_com, key_one: :gitlab_com, key_two: :github_com }
    ```

1. If you followed the above 2 steps, the new comparison page should be reachable
   under `/comparison/gitlab-vs-competitor.html` and you should see it in the
   dropdown menu. The last thing you need to do is create the PDF. Follow the
   info in [creating comparison PDFs](pdf.md#comparison-pdfs).

[comp]: https://about.gitlab.com/comparison/

## Create or update the solutions pages (under `/solutions`)

Every feature should have a solution. The list of the existing solutions can be
found at [`data/solutions.yml`](../data/solutions.yml).

To create a new one solution:

1. Edit [`data/solutions.yml`](../data/solutions.yml) and add the new solution.
   You can copy the format of an existing one.

    >**Note:**
    There are two categories of solutions: 1) "Phases of the software development
    life-cycle" and 2) "Quality attributes of GitLab". The features index page
    lists them both. The first ones are taken from [`data/solutions.yml`](../data/solutions.yml),
    so when you add a new solution that belongs to the "Phases of the software
    development life-cycle", make sure to also update the value in
    `source/features/index.html.haml` (`data.solutions.solutions.take(7).each` and
    `data.solutions.solutions.drop(7).each`).

1. Create `source/solutions/<solution-name>/index.html.haml`. You can copy the
   format of an existing solution to get started.

To update an existing solution, just edit [`data/solutions.yml`](../data/solutions.yml).

## Update the Return on Investment calculator page (under `/roi`)

The [`/roi`](https://about.gitlab.com/roi/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

The table below tries to describe the minimum values a feature for the ROI
calculator will need in order to appear under `/roi`.

| Features values | Description |
| --------------- | ----------- |
| `title` | The distinct title of a feature. |
| `link` | The link pointing either to the feature page or the docs. |
| `shorthand` | This is the id that it's used for the url parameters so users can share their ROI calculations. It needs to have underscores (`_`) instead of spaces in order for it to work.  |
| `hours_per_incident` | The number of hours that an incident will take before it's solved. It can be skipped but it will make the calculator return 0 for that feature. |
| `incidents_per_year` | The number of times that an incident will happen in a year. It can be skipped but it will make the calculator return 0 for that feature. |
| `roi_case` | The description of the case that makes use of the feature that's described on the title, if this is skipped the feature will not be considered for the roi features table. |
