---
title: "GitLab Critical Security Update Pre-Announcement"
date: 2017-08-04
author: Brian Neel
author_twitter: b0bby_tables
categories: Security
tags: security, gitlab
---

On Thursday, August 10th, 2017 at 18:00 UTC, we will publish a critical GitLab
security update. More details will be forthcoming on [our blog], including
which versions of GitLab are affected.

We recommend installations running affected versions to upgrade as soon as the 
new releases are available. Please forward this alert to the appropriate people 
at your organization and have them subscribe to [Security Notices].

Please note the 18:00 UTC release time. This is different from the 23:59 UTC
release time used for previous critical security releases. This change will be 
explained in the release blog post.

[our blog]: https://about.gitlab.com/blog
[Security Notices]: https://about.gitlab.com/contact/

<!-- more -->
